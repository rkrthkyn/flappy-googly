// platformer_basic.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <chrono>
#include <SFML/Graphics.hpp>
#include "obstacles.h"
#include <assert.h>
#include <stdio.h>
#include <list>
#include <windows.h>
#include <ctime>
#include <iostream>
#include <fstream>

//Simple pop-front method for std::vectors
//not efficient for large lists!
template<typename T>
void pop_front(std::vector<T>& vec)
{
	assert(!vec.empty());
	vec.erase(vec.begin());
}

int main() {
	/*--------------------------
	Game attributes
	---------------------------*/
	bool resetGame = false;
	int nAttemptCount = 0;
	int nFlapCount = 0;
	int nMaxFlapCount = 0;
	/*--------------------------
	Obstacle attributes
	---------------------------*/
	float obstPos = 100;
	float obstacleWidth = 50;
	float passageWidth = 200;
	bool boolRun = true;
	bool oppositeHurdle = true;
	float hurdle1_2ht, hurdle2_2ht, hurdle3_2ht, hurdle4_2ht, hurdle5_2ht;
	/*--------------------------
	Doggo attributes
	---------------------------*/
	float doggoPosition = 0.0f;
	float doggoVelocity = 0.0f;
	float doggoAcceleration = 0.0f;
	float myGravity = 100.0f;
	float sectionSpan;
	bool doggoCollision = false;
	/*--------------------------
	Window attributes
	---------------------------*/
	float windowDimensionX = 1000;
	float windowDimensionY = 700;
	sf::RenderWindow window;
	window.setFramerateLimit(120);
	sf::Vector2i centerWindow((sf::VideoMode::getDesktopMode().width / 2)
		- 445, (sf::VideoMode::getDesktopMode().height / 2) - 480);
	window.create(sf::VideoMode(windowDimensionX, windowDimensionY),
		"One flappy good-boi", sf::Style::Titlebar | sf::Style::Close);
	window.setPosition(centerWindow);
	window.setKeyRepeatEnabled(true);
	/*--------------------------
	World attributes
	---------------------------*/
	std::vector<float> obstacleHt(5, 0.0f);
	std::vector < sf::RectangleShape> obstacleVector;
	sectionSpan = (windowDimensionX / (obstacleHt.size() - 1)); //ObstacleHt to be declared before this!
	float frameCurrentPosition = 0.0f;
	float myRandomValue = 0.0f;
	/*--------------------------
	Player style attributes
	---------------------------*/
	int textureFlag = 0;
	sf::Texture textureChill, textureEcstatic, textureSad;
	textureChill.loadFromFile("chill.png");
	textureEcstatic.loadFromFile("ecstatic.png");
	textureSad.loadFromFile("sad.png");
	sf::Sprite googly;
	googly.setTexture(textureChill);
	googly.setScale({ 0.5,0.5 });

	/*
	sf::RectangleShape googly;
	googly.setSize({ 40,40 });
	googly.setPosition({ 500,350 });
	*/

	/*--------------------------
	Obstacle attributes II
	---------------------------*/
	sf::Texture aggiePillarTexture;
	sf::RectangleShape hurdle;
	aggiePillarTexture.loadFromFile("pillar.png");
	hurdle.setTexture(&aggiePillarTexture);
	hurdle.setSize({ obstacleWidth, 0 });
	obstacleVector.reserve(2 * obstacleHt.size());
	for (int i = 0; i <= 2 * obstacleHt.size() - 1; i++)
	{
		obstacleVector.push_back(hurdle);
	}
	/*--------------------------
	Score keeper attributes
	---------------------------*/
	sf::Text textObject;
	std::vector < sf::Text> textVector;
	textVector.reserve(2);
	sf::Font MyFont;
	if (!MyFont.loadFromFile("arial.ttf")) {}
	for (int i = 0; i < 2; i++) { textVector.push_back(textObject); }
	for (float j = 0; j < 2; j++) {
		textVector[j].setPosition({ 10 + 40 * (3 * j),10 });
		textVector[j].setFont(MyFont);
		textVector[j].setCharacterSize(24);
		textVector[j].setFillColor(sf::Color::Red);
	}


	/*--------------------------
	Secondary task attributes
	---------------------------*/

	sf::Text mathQuestion, userResponse;
	int numA, numB, numC, inputNumber, userValue, queryValue, taskCounter = 0;
	int debouncer = 0;
	mathQuestion.setPosition(30, windowDimensionY - 50);
	mathQuestion.setFont(MyFont);
	mathQuestion.setCharacterSize(24);
	mathQuestion.setFillColor(sf::Color::Green);

	userResponse.setPosition(240, windowDimensionY - 50);
	userResponse.setFont(MyFont);
	userResponse.setCharacterSize(24);
	userResponse.setFillColor(sf::Color::Yellow);

	std::string currentNumber = "";
	bool bMathResponse = false;
	bool bSecondaryTask = false;
	numA = (rand() % 20) + 1;
	numB = (rand() % 20) + 1;
	numC = (rand() % 20) + 1;

	bool keyReleased0 = true;
	bool keyReleased1 = true;
	bool keyReleased2 = true;
	bool keyReleased3 = true;
	bool keyReleased4 = true;
	bool keyReleased5 = true;
	bool keyReleased6 = true;
	bool keyReleased7 = true;
	bool keyReleased8 = true;
	bool keyReleased9 = true;
	bool keyReleasedReturn = true;
	/*--------------------------
	Elapsed time counter
	---------------------------*/
	int counter = 0;
	auto tp1 = std::chrono::system_clock::now();
	auto tp2 = std::chrono::system_clock::now();
	/*--------------------------
	File IO
	---------------------------*/
	std::ofstream myfile;
	myfile.open("yohan_math_only.csv");
	std::vector<int> flapCountVector;
	std::vector<int> mathCountVector;
	/*--------------------------
	The mother loop!
	---------------------------*/
	while (window.isOpen()) {
		/*--------------------------
		Game reset criteria
		---------------------------*/
		if (resetGame) {
			std::cout << "Uh oh....reset game" << std::endl;
			flapCountVector.push_back(nFlapCount);
			mathCountVector.push_back(taskCounter);
			srand(time(0));
			tp1 = std::chrono::system_clock::now();
			tp2 = std::chrono::system_clock::now();
			resetGame = false;
			nFlapCount = 0;
			nAttemptCount++;
			doggoPosition = windowDimensionY / 2; //hurr-durr!
			doggoVelocity = 0.0f;
			doggoAcceleration = 0.0f;
			for (int i = 0; i <= (obstacleVector.size() - 1); i++)
			{
				obstacleVector[i].setSize({ obstacleWidth, 0 });
			}
			std::fill(obstacleHt.begin(), obstacleHt.end(), 0);

			userResponse.setString("");
			window.draw(userResponse);
			numA = (rand() % 20) + 1;
			numB = (rand() % 20) + 1;
			numC = (rand() % 20) + 1;
			queryValue = numA + numB + numC;
			bMathResponse = false;
			keyReleased0 = true;
			keyReleased1 = true;
			keyReleased2 = true;
			keyReleased3 = true;
			keyReleased4 = true;
			keyReleased5 = true;
			keyReleased6 = true;
			keyReleased7 = true;
			keyReleased8 = true;
			keyReleased9 = true;
			keyReleasedReturn = true;
			debouncer = 0;
			userValue = 0;
			taskCounter = 0;
			currentNumber = "";
			std::cout << ".......reset complete" << std::endl;
		}
		else {
			counter++;
			tp2 = std::chrono::system_clock::now();
			std::chrono::duration<float> elapsedTime = tp2 - tp1;
			tp1 = tp2;
			float fElapsedTime = elapsedTime.count();
			sf::Event Event;


			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space) && doggoVelocity >= myGravity / 5.0f) {
				textureFlag = 0;
				//Character control features go here!
				doggoAcceleration = 0.0f;
				doggoVelocity = -myGravity;
				googly.setTexture(textureEcstatic);
				nFlapCount++;
				//Scoring features
				if (nFlapCount > nMaxFlapCount)
					nMaxFlapCount = nFlapCount;
			}
			else
			{
				textureFlag++;
				doggoAcceleration += myGravity * fElapsedTime;
				if (textureFlag > 1000) {
					googly.setTexture(textureChill);
				}
			}
			if (doggoAcceleration >= myGravity)
			{
				doggoAcceleration = myGravity;
			}

			doggoVelocity += doggoAcceleration * fElapsedTime;
			doggoPosition += doggoVelocity * fElapsedTime;
			frameCurrentPosition += 60.0f * fElapsedTime; 		//optimize the multiplication constant here!

																/*--------------------------
																Re-position obstacles
																---------------------------*/
			int iter2 = 0;
			if (frameCurrentPosition > sectionSpan) {
				frameCurrentPosition -= sectionSpan;
				myRandomValue = rand() % 500 + 1;
				if (myRandomValue < 10) { myRandomValue = 0; }
				pop_front(obstacleHt);
				obstacleHt.push_back(myRandomValue);
			}

			//obstacle display component :
			//Set XX1, and XX2 correctly!
			for (int i = 0; i < 9; i++) {
				if (i % 2 == 0) {
					//update heights (shift one left)
					obstacleVector[i].setSize({ obstacleWidth, obstacleHt[iter2] });
					if (obstacleHt[iter2] != 0) {
						obstacleVector[i + 1].setSize({ obstacleWidth,
							windowDimensionY - obstacleHt[iter2] - passageWidth });
					}
					else { obstacleVector[i + 1].setSize({ obstacleWidth, 0 }); }
					//update position
					float XX1 = iter2 * sectionSpan + 10 - frameCurrentPosition;
					obstacleVector[i].setPosition({ XX1,0 });
					obstacleVector[i + 1].setPosition({ XX1, obstacleHt[iter2] + passageWidth });
					iter2++;
				}
			}
			//Doggo position updates
			googly.setPosition({ 500, doggoPosition });
			//Doggo collision detection
			if (counter > 5000) {
				resetGame = doggoPosition < 2 ||
					doggoPosition > windowDimensionY - 2;
			}
			for (int i = 0; i <= (obstacleVector.size() - 1); i++) {
				if (googly.getGlobalBounds().intersects(obstacleVector[i].getGlobalBounds()))
				{
					resetGame = true;
				}
			}

			/*--------------------------
			Math task response
			---------------------------*/
			queryValue = numA + numB + numC;
			//Super ineffecient method :(

			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad0) && keyReleased0) {
				inputNumber = 0;
				bMathResponse = true;
				keyReleased0 = false;
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad1) && keyReleased1) {
				inputNumber = 1;
				bMathResponse = true;
				keyReleased1 = false;
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad2) && keyReleased2) {
				inputNumber = 2;
				bMathResponse = true;
				keyReleased2 = false;

			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad3) && keyReleased3) {
				inputNumber = 3;
				bMathResponse = true;
				keyReleased3 = false;

			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad4) && keyReleased4) {
				inputNumber = 4;
				bMathResponse = true;
				keyReleased4 = false;


			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad5) && keyReleased5) {
				inputNumber = 5;
				bMathResponse = true;
				keyReleased5 = false;


			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad6) && keyReleased6) {
				inputNumber = 6;
				bMathResponse = true;
				keyReleased6 = false;


			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad7) && keyReleased7) {
				inputNumber = 7;
				bMathResponse = true;
				keyReleased7 = false;


			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad8) && keyReleased8) {
				inputNumber = 8;
				bMathResponse = true;
				keyReleased8 = false;

			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad9) && keyReleased9) {
				inputNumber = 9;
				bMathResponse = true;
				keyReleased9 = false;
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Return) && keyReleasedReturn)
			{
				if (currentNumber == "") {
					currentNumber = "-1";
				}
				userValue = stoi(currentNumber);
				std::cout << userValue << std::endl;
				if (userValue != queryValue)
				{
					std::cout << "uh oh...wrong input" << std::endl;
					resetGame = true;
				}
				else {
					userResponse.setString("");
					window.draw(userResponse);
					currentNumber = "";
					nFlapCount++;
					numA = (rand() % 20) + 1;
					numB = (rand() % 20) + 1;
					numC = (rand() % 20) + 1;
					taskCounter++;
					std::cout << "Math task count:" << taskCounter << std::endl;
				}
				keyReleasedReturn = false;
			}
			if (debouncer > 5000) {
				keyReleased0 = true;
				keyReleased1 = true;
				keyReleased2 = true;
				keyReleased3 = true;
				keyReleased4 = true;
				keyReleased5 = true;
				keyReleased6 = true;
				keyReleased7 = true;
				keyReleased8 = true;
				keyReleased9 = true;
				keyReleasedReturn = true;
				debouncer = 0;
			}
			else { debouncer++; }

			if (bMathResponse) {
				currentNumber += std::to_string(inputNumber);
				userResponse.setString(currentNumber);
			}

			bMathResponse = false;

			if (resetGame) { googly.setTexture(textureSad); }

			/*--------------------------
			Event loop
			---------------------------*/
			while (window.pollEvent(Event)) {
				switch (Event.type) {



				case sf::Event::Closed:
					for (int i = 0; i < flapCountVector.size(); i++) { myfile << flapCountVector[i] << "," << mathCountVector[i] << std::endl; }
					window.close();
				}
			}


			window.clear();
			window.draw(googly);
			for (int i = 0; i <= (obstacleVector.size() - 1); i++) {
				window.draw(obstacleVector[i]);
			}
			textVector[0].setString("Score: " + std::to_string(nFlapCount));
			textVector[1].setString("Attempt: " + std::to_string(nAttemptCount));
			mathQuestion.setString("What is:" + std::to_string(numA) + "+" +
				std::to_string(numB) + "+" + std::to_string(numC) + "=?");
			window.draw(textVector[0]);
			window.draw(textVector[1]);
			window.draw(mathQuestion);
			window.draw(userResponse);
			window.display();
			if (resetGame) { Sleep(4000); }
		}

	}
}

