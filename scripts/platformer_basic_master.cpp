// platformer_basic.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <chrono>
#include <SFML/Graphics.hpp>
#include "puff.h"
#include "obstacles.h"
#include <assert.h>
#include <stdio.h>
#include <list>
#include <windows.h>
#include <iostream>
#include <fstream>

//Simple pop-front method for std::vectors
//not efficient for large lists!

template<typename T>
void pop_front(std::vector<T>& vec)
{
	assert(!vec.empty());
	vec.erase(vec.begin());
}

int main() {

	/*--------------------------
	Game attributes
	---------------------------*/
	bool resetGame = false;
	int nAttemptCount = 0;
	int nFlapCount = 0;
	int nMaxFlapCount = 0;
	/*--------------------------
	Obstacle attributes
	---------------------------*/
	float obstPos = 100;
	float obstacleWidth = 50;
	float passageWidth = 200;
	bool boolRun = true;
	bool oppositeHurdle = true;
	float hurdle1_2ht, hurdle2_2ht, hurdle3_2ht, hurdle4_2ht, hurdle5_2ht;
	/*--------------------------
	Doggo attributes
	---------------------------*/
	float doggoPosition = 0.0f;
	float doggoVelocity = 0.0f;
	float doggoAcceleration = 0.0f;
	float myGravity = 100.0f;
	float sectionSpan;
	bool doggoCollision = false;
	/*--------------------------
	Window attributes
	---------------------------*/
	float windowDimensionX = 1000;
	float windowDimensionY = 700;
	sf::RenderWindow window;
	window.setFramerateLimit(120);
	sf::Vector2i centerWindow((sf::VideoMode::getDesktopMode().width / 2)
		- 445, (sf::VideoMode::getDesktopMode().height / 2) - 480);
	window.create(sf::VideoMode(windowDimensionX, windowDimensionY),
		"One flappy good-boi", sf::Style::Titlebar | sf::Style::Close);
	window.setPosition(centerWindow);
	window.setKeyRepeatEnabled(true);
	/*--------------------------
	World attributes
	---------------------------*/
	std::vector<float> obstacleHt(5, 0.0f);
	std::vector < sf::RectangleShape> obstacleVector;
	sectionSpan = (windowDimensionX / (obstacleHt.size() - 1)); //ObstacleHt to be declared before this!
	float frameCurrentPosition = 0.0f;
	float myRandomValue = 0.0f;
	/*--------------------------
	Player style attributes
	---------------------------*/
	int textureFlag = 0;
	sf::Texture textureChill, textureEcstatic, textureSad;
	textureChill.loadFromFile("chill.png");
	textureEcstatic.loadFromFile("ecstatic.png");
	textureSad.loadFromFile("sad.png");
	sf::Sprite googly;
	googly.setTexture(textureChill);
	googly.setScale({ 0.5,0.5 });

	/*
	sf::RectangleShape googly;
	googly.setSize({ 40,40 });
	googly.setPosition({ 500,350 });
	*/

	/*--------------------------
	Obstacle attributes II
	---------------------------*/
	sf::Texture aggiePillarTexture;
	sf::RectangleShape hurdle;
	aggiePillarTexture.loadFromFile("pillar.png");
	hurdle.setTexture(&aggiePillarTexture);
	hurdle.setSize({ obstacleWidth, 0 });
	obstacleVector.reserve(2 * obstacleHt.size());
	for (int i = 0; i <= 2 * obstacleHt.size() - 1; i++)
	{
		obstacleVector.push_back(hurdle);
	}
	/*--------------------------
	Score keeper attributes
	---------------------------*/
	sf::Text textObject;
	std::vector < sf::Text> textVector;
	textVector.reserve(2);
	sf::Font MyFont;
	if (!MyFont.loadFromFile("arial.ttf")) {}
	for (int i = 0; i < 2; i++) { textVector.push_back(textObject); }
	for (float j = 0; j < 2; j++) {
		textVector[j].setPosition({ 10 + 40 * (3 * j),10 });
		textVector[j].setFont(MyFont);
		textVector[j].setCharacterSize(24);
		textVector[j].setFillColor(sf::Color::Red);

	}
	/*--------------------------
	Elapsed time counter
	---------------------------*/
	int counter = 0;
	auto tp1 = std::chrono::system_clock::now();
	auto tp2 = std::chrono::system_clock::now();

	/*--------------------------
	File IO
	---------------------------*/
	std::ofstream myfile;
	myfile.open("yohan_lo_tempo.csv");
	std::vector<int> flapCountVector;


	/*--------------------------
	The mother loop!
	---------------------------*/
	while (window.isOpen()) {
		/*--------------------------
		Game reset criteria
		---------------------------*/
		if (resetGame) {
			std::cout << "Uh oh....reset game" << std::endl;
			flapCountVector.push_back(nFlapCount);
			tp1 = std::chrono::system_clock::now();
			tp2 = std::chrono::system_clock::now();
			resetGame = false;
			nFlapCount = 0;
			nAttemptCount++;
			doggoPosition = windowDimensionY / 2; //hurr-durr!
			doggoVelocity = 0.0f;
			doggoAcceleration = 0.0f;
			for (int i = 0; i <= (obstacleVector.size() - 1); i++)
			{
				obstacleVector[i].setSize({ obstacleWidth, 0 });
			}
			std::fill(obstacleHt.begin(), obstacleHt.end(), 0);
			std::cout << ".......reset complete" << std::endl;
		}
		else {
			counter++;
			tp2 = std::chrono::system_clock::now();
			std::chrono::duration<float> elapsedTime = tp2 - tp1;
			tp1 = tp2;
			float fElapsedTime = elapsedTime.count();

			sf::Event Event;
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space) && doggoVelocity >= myGravity / 5.0f) {
				textureFlag = 0;
				//Character control features go here!
				doggoAcceleration = 0.0f;
				doggoVelocity = -myGravity;
				googly.setTexture(textureEcstatic);
				nFlapCount++;
				//Scoring features
				if (nFlapCount > nMaxFlapCount)
					nMaxFlapCount = nFlapCount;
			}
			else
			{
				textureFlag++;
				doggoAcceleration += myGravity * fElapsedTime;
				if (textureFlag > 1000) {
					googly.setTexture(textureChill);
				}
			}
			if (doggoAcceleration >= myGravity)
			{
				doggoAcceleration = myGravity;
			}

			doggoVelocity += doggoAcceleration * fElapsedTime;
			doggoPosition += doggoVelocity * fElapsedTime;
			frameCurrentPosition += 60.0f * fElapsedTime; 		//optimize the multiplication constant here!

																/*--------------------------
																Re-position obstacles
																---------------------------*/
			int iter2 = 0;
			if (frameCurrentPosition > sectionSpan) {
				frameCurrentPosition -= sectionSpan;
				myRandomValue = rand() % 500 + 1;
				if (myRandomValue < 10) { myRandomValue = 0; }
				pop_front(obstacleHt);
				obstacleHt.push_back(myRandomValue);
			}

			//obstacle display component :
			//Set XX1, and XX2 correctly!
			for (int i = 0; i < 9; i++) {
				if (i % 2 == 0) {
					//update heights (shift one left)
					obstacleVector[i].setSize({ obstacleWidth, obstacleHt[iter2] });
					if (obstacleHt[iter2] != 0) {
						obstacleVector[i + 1].setSize({ obstacleWidth,
							windowDimensionY - obstacleHt[iter2] - passageWidth });
					}
					else { obstacleVector[i + 1].setSize({ obstacleWidth, 0 }); }
					//update position
					float XX1 = iter2 * sectionSpan + 10 - frameCurrentPosition;
					obstacleVector[i].setPosition({ XX1,0 });
					obstacleVector[i + 1].setPosition({ XX1, obstacleHt[iter2] + passageWidth });
					iter2++;
				}
			}
			//Doggo position updates
			googly.setPosition({ 500, doggoPosition });
			//Doggo collision detection
			if (counter > 5000) {
				resetGame = doggoPosition < 2 ||
					doggoPosition > windowDimensionY - 2;
			}
			for (int i = 0; i <= (obstacleVector.size() - 1); i++) {
				if (googly.getGlobalBounds().intersects(obstacleVector[i].getGlobalBounds()))
				{

					resetGame = true;
				}
			}

			if (resetGame) { googly.setTexture(textureSad); }

			//Event Loop:
			while (window.pollEvent(Event)) {
				switch (Event.type) {

				case sf::Event::Closed:
					for (int i = 0; i < flapCountVector.size(); i++) { myfile << flapCountVector[i] << std::endl; }
					window.close();
				}
			}

			//Figure out drawing function - correctly!
			window.clear();
			window.draw(googly);
			for (int i = 0; i <= (obstacleVector.size() - 1); i++) {
				window.draw(obstacleVector[i]);
			}
			textVector[0].setString("Score: " + std::to_string(nFlapCount));
			textVector[1].setString("Attempt: " + std::to_string(nAttemptCount));
			window.draw(textVector[0]);
			window.draw(textVector[1]);
			window.display();
			if (resetGame) { Sleep(4000); }
		}

	}
}

